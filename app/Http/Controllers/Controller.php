<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function homepage(){
        $var = "Homepage do sistema de gestão para o grupo de imvestimento";
        return view('welcome', [
            'title' => $var
        ]);
    }

    public function cadastro(){
        echo "Tela de cadastro";
    }

/*
* method to user login VIEW
* ========================================
*/
    public function fazerLogin(){
        
        return view('user.login');
        
    }
}
