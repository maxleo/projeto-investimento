<?php

use Illuminate\Database\Seeder;
use App\Entities\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'cpf' => '11122233345',
            'name' => 'Lendro',
            'phone' =>'3512349999',
            'birth' =>'1996-04-01',
            'gender' =>'M',
            'email' =>'maxleo2505@gmail.com',
            'password' =>env('PASSWORD_HASH') ? bcrypt('123456') : '123456',
        ]);


        // $this->call(UsersTableSeeder::class);
    }
}
